#include <iostream>
#include <map>
#include <stdlib.h>
#include "SnakeLadderGame.h"
using namespace std;

class SnackLadderGameUT{
    public:
    string TestRoleMyDice(MySnakeLadderGame &obj){
        obj.diceType = true;
        int rolled_number = 0;
        for(int i=0;i<10;i++){
            rolled_number = obj.roleMyDice(obj.diceType);
            if (rolled_number < 1 || rolled_number > 6) return "Failed.";
        }

        obj.diceType = false;
        for(int i=0;i<10;i++){
            rolled_number = obj.roleMyDice(obj.diceType);
            if (rolled_number != 2 && rolled_number != 4 && rolled_number != 6) return "Failed.";
        }
        return "Passed.";
    }
    string TestAddSnake(MySnakeLadderGame &obj){
        obj.addSnake(6,2);
        obj.currPosition = 5;
        obj.makeMyMove(1);
        if (obj.getMyCurrentPosition() != 2) return "Failed.";
        return "Passed.";
    }
    string TestAddLadder(MySnakeLadderGame &obj){
        obj.addLadder(7,2);
        obj.currPosition = 1;
        obj.makeMyMove(1);
        if (obj.getMyCurrentPosition() != 7) return "Failed.";
        return "Passed.";
    }
};

int main() {
  SnackLadderGameUT test;
  MySnakeLadderGame myGame;  // Create an object of MyClass
  myGame.debugFlag = false;
  cout << "\nTest Case Role My Dice:"<<test.TestRoleMyDice(myGame);
  cout << "\nTest Case Add Snake:"<<test.TestAddSnake(myGame);
  cout << "\nTest Case Add Ladder:"<<test.TestAddLadder(myGame);
  // We can add other test case also like,
  // 1.) Reaching to position 98 and then dice shows 4. (User shouldn't move)
  // 2.) Trying to add a snack mounth & ladder's climbing position at same position (Shouldn't be allowed)
  // etc..
  return 0;
}