#include <iostream>
#include <map>
#include <stdlib.h>
#include "SnakeLadderGame.h"

using namespace std;

int main() {
  MySnakeLadderGame myObj;  // Create an object of MyClass
  myObj.initGame();
  bool continueGame = true;
  while(continueGame){
    continueGame = myObj.playMyTurn();
  }
  return 0;
}