# SnakeLadderGame

**Details** A Single player Snake & Ladder Game where we can add number of snakes & ladder to set the game up. 
User can crook the dice to show only even numbers.


1. To build the project do below:
- Checkout all 3 files.
- do g++ PlayTheGame.cpp -o Play
- do g++ SnakeLadderUT.cpp -o SnakeLadderUT

2. To run the UT do : ./SnakeLadderUT
3. To run the Game do: ./Play and follow the instructions for setup.
