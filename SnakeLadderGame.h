#include <iostream>
#include <map>
#include <stdlib.h>
using namespace std;

class MySnakeLadderGame {
  public:
    bool debugFlag = true;
    int currPosition;
    bool diceType; // True-> simple dice, false-> event numbered (kharaab) dice
    map<int, int> snake; // <head , tail> b'coz we need a const search for head of a snake
    map<int, int> ladder; // <tail, head> b'coz we need to const search for tail of a ladder to climb on it.

  // Returns a random number based on dice type
  int roleMyDice(bool diceType){
    if (diceType) return (rand() % 6) + 1;
    else return ((rand() % 3) + 1) *2;
  }

  // Adds a snake in the game
  bool addSnake(int headOfSnake, int tailOfSnake){
    if (headOfSnake < 2 || headOfSnake > 100 || tailOfSnake < 1 || tailOfSnake > 99 || tailOfSnake >= headOfSnake){
        cout << "Invalid snake coordinates.. Please read the instructions again\n";
        return false;
    }

    // a snake's head & ladder's tail can't be at same point.
    if (ladder.find(headOfSnake) != ladder.end()){
        cout << "snake's head can't be at ladder's starting point\n";
        return false;
    }
    else{
        snake[headOfSnake] = tailOfSnake;
        if (debugFlag)
        cout<<"Successfully added a snake in your game.\n";
    }
    return true;
  }

  // Adds a ladder in the game
  bool addLadder(int lendingLoc, int takeOffLoc){
    if (lendingLoc < 2 || lendingLoc > 100 || takeOffLoc < 1 || takeOffLoc > 99 || lendingLoc <= takeOffLoc){
        cout << "Invalid ladder coordinates.. Please read the instructions again\n";
        return false;
    }

    // a snake's head & ladder's tail can't be at same point.
    if (snake.find(takeOffLoc) != snake.end()){
        cout << "ladder's starting point can't be at snake's head\n";
        return false;
    }
    else {
        ladder[takeOffLoc] = lendingLoc;
        if (debugFlag)
        cout<<"Successfully added a ladder in your game.\n";
    }
    return true;
  }

  // Returns the final location of the player if found ladder otherwise returns 0
  int IsLadder(int position){
      if (ladder.find(position) != ladder.end())
        return ladder[position];
      else return 0;
  }

  // Returns the final location of the player if bitten by snack otherwise returns 0
  int IsBittenBySnake(int position){
      if (snake.find(position) != snake.end())
        return snake[position];
      else return 0;
  }

  int getMyCurrentPosition(){
    return currPosition;
  }

  // moves a player by  given steps, return false if player has won after these many steps else returns true
  bool makeMyMove(int steps){
    if (currPosition + steps > 100) {
        cout << "You can't make that move at:"<<currPosition<<endl;
        return true;
    }

    currPosition += steps;

    if (IsBittenBySnake(currPosition)) {
        if (debugFlag)
        cout<<"Oouch.. you got bitten by a snake at:"<<currPosition<<endl;
        currPosition = snake[currPosition];
    } else if (IsLadder(currPosition)){
        if (debugFlag)
        cout<< "Woow.. you found a ladder at:"<<currPosition<<endl;
        currPosition = ladder[currPosition];
    }
    if (debugFlag)
    cout<<"You reached to:"<< currPosition<<endl;
    if (currPosition == 100){
        cout<<"Congratulations you won the game..!!!\n";
        return false;
    }
    else return true;
  }

  bool playMyTurn(){
    int number = roleMyDice(diceType);
    if (debugFlag)
    cout<<"You rolled:"<< number <<endl;

    return makeMyMove(number);
  }

  void initGame(){
    cout << "###### Starting Snake & ladder Game ###############\n";
    diceType = 1;
    currPosition = 0;
    int choice;
    int head, tail;
    while(true){
        cout<<"Choose option..\n";
        cout<<"1.) Add a snake\n";
        cout<<"2.) Add a ladder\n";
        cout<<"3.) Change dice polarity\n";
        cout<<"4.) Start the game\n";
        cout<<"User Input:";
        cin >> choice;
        if (choice == 4) {
            break;
        }
        else if (choice == 1){
            cout << "Enter Head Position of snake (Snake bitting location):";
            cin >> head;
            cout << "Enter Tail Position of snake:";
            cin >> tail;
            addSnake(head, tail);
        }
        else if (choice == 2){
            cout << "Enter Head Position of ladder(destination of ladder):";
            cin >> head;
            cout << "Enter Tail Position of ladder(Source of ladder from where user can climb it):";
            cin >> tail;
            addLadder(head, tail);
        }
        else if (choice == 3){
            if (diceType) {
                diceType = false;
                cout<<"Now your dice is crooked.\n";
            }
            else {
                diceType = true;
                cout<<"Now your dice is unbiased.\n";
            }
        }
        else
        {
            cout << "Invalid Choice, Please try again..!!!\n";
            cout << "If you are getting this frequently then contact support at dheeraj.madawat09@gmail.com";
        }
    }
  }
};

